#ifndef READ_FILE_FUNCTIONS
#define READ_FILE_FUNCTIONS

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

void readFile(ifstream& FILE);

#endif
