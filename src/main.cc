#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

#include "read_file_functions.h"

using namespace std;

int main(int argc, char* argv[]) {

	if (argc != 2) {
		cout << "main - error, only 1 input file needed" << endl;
		return(1);
	}
	
	const string dataFile(argv[1]);
	ifstream FILE;
	FILE.open(dataFile.c_str());
	
	if (!FILE.good()) {
		cout << "main - error, cannot open file" << endl;
		return(1);
	}

	readFile(FILE);

	return 0;
}
